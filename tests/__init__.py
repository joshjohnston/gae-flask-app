import functools
import unittest2

from fixture import GoogleDatastoreFixture, DataSet
from fixture.loadable.google_datastore_loadable import EntityMedium
from fixture.style import NamedDataStyle
from google.appengine.ext import ndb

from app import app


class BaseTestCase(unittest2.TestCase):
    def setUp(self):
        app.config['TESTING'] = True
        app.config['CSRF_ENABLED'] = False
        app.testing = True
        self.app = app.test_client()
        # the bare wsgi app under test (no wrapper around)
        self.target_app = app

    def assertResponseOK(self, resp, msg=None):
        self.assertEquals(200, resp.status_int, msg)

    def tearDown(self):
        if hasattr(self, 'data'):
            self.data.teardown()

    def with_data(self, datasets, model_env):
        if hasattr(self, 'data'):
            raise AttributeError('data already set')
        self.data = _load_data_sets(model_env, datasets)


def with_data(datasets, models=None):
    """Decorator for testcase methods that loads `datasets` before

    running the tests based on `models` module.

    """
    def decorator(test_func):
        @functools.wraps(test_func)
        def inner_wrapper(self):
            _datasets = datasets
            if isinstance(datasets, basestring):
                import sys
                mod = sys.modules[self.__class__.__module__]
                _datasets = getattr(mod, datasets)
            data = _load_data_sets(models, _datasets)
            test_func(self)
            data.teardown()
        return inner_wrapper
    return decorator


def _load_data_sets(models, datasets):
    datafixture = GoogleDatastoreFixture(
        env=models,
        style=NamedDataStyle(),
        medium=_NDBEntityMedium
    )
    data = datafixture.data(*datasets)
    data.setup()
    return data


class KapxDataSet(DataSet):

    _reserved_attr = DataSet._reserved_attr + ('as_dict',)

    def _public_dir(self, row, fields):
        for name in dir(row):
            if name.startswith("_") or name in row._reserved_attr:
                continue
            if fields:
                if name in fields:
                    yield name, getattr(row, name)
            else:
                yield name, getattr(row, name)

    def as_dict(self, fields=()):
        """Return a dict representation of the Dataset"""
        return dict((k, dict(self._public_dir(v, fields))) for k,v in self)


class _NDBEntityMedium(EntityMedium):

    def _entities_to_keys(self, mylist):
        """Converts an array of datastore objects to an array of keys.

        if the value passed in is not a list, this passes it through as is
        """
        if type(mylist) is list:
            if all(map(lambda x: hasattr(x, 'key'), mylist)):
                return [ent.key if not hasattr(ent.key, '__call__') else ent.key() for ent in mylist]
        # default to db version
        return super(_NDBEntityMedium, self)._entities_to_keys(mylist)

    def clear(self, obj):
        """Delete this entity from the Datastore"""
        if isinstance(obj, ndb.Model):
            obj.key.delete()
        else:  # fall back to db version
            super(_NDBEntityMedium, self).clear(obj)