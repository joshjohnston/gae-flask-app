import datetime
from app.models import BaseModel
from app.home.models import Hello
from google.appengine.api.datastore_errors import BadValueError
from tests import BaseTestCase


class TestBaseModel(BaseTestCase):
    # Tell the NoseGAE plugin that this test uses the datastore
    nosegae_datastore_v3 = True

    def test_get_id(self):
        """Tests that BaseModel.get_id returns the ID from the ndb.Key"""
        entity = BaseModel()
        entity.put()
        self.assertEqual(entity.get_id(), entity.key.id())

    def test_get_by_ids(self):
        """Tests fetching a collection of entities by their ID values"""
        entities = [BaseModel().put(), BaseModel().put()]
        saved = BaseModel.get_by_ids([entity.id() for entity in entities])
        self.assertEqual(len(entities), len(saved))
        for entity in saved:
            self.assertIsInstance(entity, BaseModel)

    def test_audit_fields(self):
        """Tests that the base model's audit fields function as expected

            BaseModel.created_datetime should only be set once on the initial put()
            BaseModel.updated_datetime should be set every time you put()
        """
        entity = BaseModel()
        entity.put()
        self.assertIsInstance(entity.created_datetime, datetime.datetime)
        self.assertIsInstance(entity.updated_datetime, datetime.datetime)
        created = entity.created_datetime
        updated = entity.updated_datetime
        entity.put()
        self.assertEqual(created, entity.created_datetime)
        self.assertNotEqual(updated, entity.updated_datetime)


class TestHello(BaseTestCase):
    # Tell the NoseGAE plugin that this test uses the datastore
    nosegae_datastore_v3 = True

    def test_to_whom_required(self):
        """Test that Hello.to_whom requires a value"""
        entity = Hello()
        try:
            entity.put()
            self.fail("to_whom accepted an empty input")
        except BadValueError:
            self.assertIsNone(entity.to_whom)