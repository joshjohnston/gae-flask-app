Flask on GAE
===========

This app has a demo module in `src/app/home` that uses Blueprint and local templates / models

# HOW-TO
* [First time set up](#markdown-header-first-time-set-up)
* [Running the app](#markdown-header-running-the-app)
* [Run Tests](#markdown-header-run-tests)


## First time set up

The first time you clone the app you will need to follow these steps to get it set up properly

### Install zc.buildout

```
gae-flask-app$ python bootstrap.py
```

### Run buildout to install dependencies (this installs the Google AppEngine SDK for you)

```
gae-flask-app$ ./bin/buildout
```
  
### Generate secret keys for sessions, etc

```
gae-flask-app$ ./bin/python generate_keys.py
```

## Running the app

### Launch the app

```
gae-flask-app$ ./bin/dev_appserver
```

### View the app

Open your browser to [http://localhost:8080](http://localhost:8080)

## Run Tests

```
gae-flask-app$ ./bin/nosetests
```




