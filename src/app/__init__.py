import os
import logging
from flask import Flask, render_template


def is_local():
    try:
        return os.environ['SERVER_SOFTWARE'].lower().startswith('dev')
    except KeyError:
        return True

app = Flask(__name__)

if is_local():
    app.config.from_object('app.settings.Development')
    # Flask-DebugToolbar (only enabled when DEBUG=True)
    from flask_debugtoolbar import DebugToolbarExtension
    toolbar = DebugToolbarExtension(app)
else:
    app.config.from_object('app.settings.Production')

# homepage module
from home import home_blueprint

app.register_blueprint(home_blueprint)


@app.errorhandler(404)
def page_not_found(e):
    """Handles any 404 Errors"""
    logging.error(e)
    return render_template('404.html'), 404


@app.errorhandler(500)
def server_error(e):
    """Handles any 500 Errors"""
    logging.error(e)
    return render_template('500.html'), 500