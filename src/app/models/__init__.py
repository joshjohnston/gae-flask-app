from google.appengine.ext import ndb


class BaseModel(ndb.Model):
    created_datetime = ndb.DateTimeProperty(auto_now_add=True)
    updated_datetime = ndb.DateTimeProperty(auto_now=True)

    def get_id(self):
        return self.key.id()

    @classmethod
    def get_by_ids(cls, ids):
        """Gets a list of cls by ID

        :param ids: The ids to fetch
        :type ids: list of str
        :return: A collection of entities
        :rtype: list of cls
        """
        return ndb.get_multi([ndb.Key(cls._get_kind(), _id) for _id in ids])