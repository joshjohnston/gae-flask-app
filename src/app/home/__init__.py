import logging
from flask import Blueprint, render_template, abort
from .models import Hello


home_blueprint = Blueprint('home', __name__, template_folder='templates')


@home_blueprint.route('/')
def index():
    hellos = [(h.get_id(), h.to_whom) for h in Hello.query()]
    return render_template('hello.html', greetings=hellos)


@home_blueprint.route('/greeting/<int:greeting_id>')
def greeting(greeting_id):
    logging.info("Fetching greeting by id %s", greeting_id)
    hi = Hello.get_by_id(greeting_id)
    if not hi:
        abort(404)
    return render_template('say_hi.html', greeting=hi.to_whom)